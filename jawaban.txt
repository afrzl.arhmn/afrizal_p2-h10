MariaDB [(none)]> create database myshop;
Query OK, 1 row affected (0.003 sec)

MariaDB [(none)]> use myshop;
Database changed
MariaDB [myshop]> CREATE TABLE users (
    ->     id INTEGER AUTO_INCREMENT PRIMARY KEY,
    ->     name VARCHAR(255),
    ->     email VARCHAR(255),
    ->     password VARCHAR(255)
    -> );
Query OK, 0 rows affected (0.014 sec)

MariaDB [myshop]> CREATE TABLE categories (
    ->     id INTEGER AUTO_INCREMENT PRIMARY KEY,
    ->     name VARCHAR(255)
    -> );
Query OK, 0 rows affected (0.009 sec)

MariaDB [myshop]> CREATE TABLE items (
    ->     id INTEGER AUTO_INCREMENT PRIMARY KEY,
    ->     name VARCHAR(255),
    ->     description VARCHAR(255),
    ->     price INTEGER,
    ->     stock INTEGER,
    ->     category_id INTEGER,
    ->     FOREIGN KEY (category_id) REFERENCES categories(id)
    -> );
Query OK, 0 rows affected (0.017 sec)

MariaDB [myshop]> INSERT INTO users (name, email, password)
    -> VALUES
    ->     ('John Doe', 'john@doe.com', 'john123'),
    ->     ('Jane Doe', 'jane@doe.com', 'jenita123');
Query OK, 2 rows affected (0.009 sec)
Records: 2  Duplicates: 0  Warnings: 0

MariaDB [myshop]> INSERT INTO categories (name)
    -> VALUES
    ->     ('gadget'),
    ->     ('cloth'),
    ->     ('men'),
    ->     ('women'),
    ->     ('branded');
Query OK, 5 rows affected (0.003 sec)
Records: 5  Duplicates: 0  Warnings: 0

MariaDB [myshop]> INSERT INTO items (name, description, price, stock, category_id)
    -> VALUES
    ->     ('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1),
    ->     ('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2),
    ->     ('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);
Query OK, 3 rows affected (0.003 sec)
Records: 3  Duplicates: 0  Warnings: 0

MariaDB [myshop]> SELECT id, name, email
    -> FROM users;
+----+----------+--------------+
| id | name     | email        |
+----+----------+--------------+
|  1 | John Doe | john@doe.com |
|  2 | Jane Doe | jane@doe.com |
+----+----------+--------------+
2 rows in set (0.005 sec)

MariaDB [myshop]> SELECT *
    -> FROM items
    -> WHERE price > 1000000;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
2 rows in set (0.003 sec)

MariaDB [myshop]> SELECT *
    -> FROM items
    -> WHERE name LIKE '%uniklo%' OR name LIKE '%watch%' OR name LIKE '%sang%';
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.003 sec)

MariaDB [myshop]> SELECT i.name, i.description, i.price, i.stock, i.category_id, c.name AS kategori
    -> FROM items i
    -> JOIN categories c ON i.category_id = c.id;
+-------------+-----------------------------------+---------+-------+-------------+----------+
| name        | description                       | price   | stock | category_id | kategori |
+-------------+-----------------------------------+---------+-------+-------------+----------+
| Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 | gadget   |
| Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 | cloth    |
| IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget   |
+-------------+-----------------------------------+---------+-------+-------------+----------+
3 rows in set (0.004 sec)

MariaDB [myshop]> UPDATE items
    -> SET price = 2500000
    -> WHERE name = 'Sumsang b50';
Query OK, 1 row affected (0.004 sec)
Rows matched: 1  Changed: 1  Warnings: 0